package com.example.demo;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface ExcelProperty {
	
	public int index(); // 指定 JavaBean 的属性对应 excel 的第几列
	
	public String format() default "yyyy-MM-dd"; // 当 JavaBean 的属性为 Date 类型时，指定 Date 的格式化模式
	
	public boolean required() default false; // 是否为必填
	
	public String[] nvl() default {}; // 数据转换，length 长度必须为偶数, {k1, v2, k2, v2, ...}：eg：男 → M，女 → F
	
}