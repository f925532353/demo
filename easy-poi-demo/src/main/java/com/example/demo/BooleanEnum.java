package com.example.demo;

public enum BooleanEnum {
	
	True0("是", Boolean.TRUE), 
	True1("Y", Boolean.TRUE), 
	True2("TRUE", Boolean.TRUE), 
	True3("1", Boolean.TRUE),
	True4("YES", Boolean.TRUE), 
	True5("T", Boolean.TRUE), 
	False0("否", Boolean.FALSE), 
	False1("N", Boolean.FALSE),
	False2("FALSE", Boolean.FALSE), 
	False3("0", Boolean.FALSE), 
	False4("NO", Boolean.FALSE),
	False5("F", Boolean.FALSE);

	private String key;
	private Boolean value;

	private BooleanEnum(String key, Boolean value) {
		this.key = key;
		this.value = value;
	}

	public String getKey() {
		return key;
	}

	public Boolean getValue() {
		return value;
	}
}
