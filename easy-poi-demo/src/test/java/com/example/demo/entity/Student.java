package com.example.demo.entity;

import java.util.Date;

import com.example.demo.ExcelProperty;

public class Student {

	@ExcelProperty(index = 0, required = true)
	private Integer sno; // 学号
	
	@ExcelProperty(index = 1)
	private String name; // 姓名
	
	@ExcelProperty(index = 2, nvl = {"男", "M", "女", "F"})
	private String sex;	// 性别
	
	@ExcelProperty(index = 3, format = "yyyy-MM-dd")
	private Date birthdayDate; // 出生日期
	
	@ExcelProperty(index = 4)
	private Double score; // 成绩
	
	@ExcelProperty(index = 5)
	private Boolean beMember; // 是否团员

	public Integer getSno() {
		return sno;
	}

	public void setSno(Integer sno) {
		this.sno = sno;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public Date getBirthdayDate() {
		return birthdayDate;
	}

	public void setBirthdayDate(Date birthdayDate) {
		this.birthdayDate = birthdayDate;
	}

	public Double getScore() {
		return score;
	}

	public void setScore(Double score) {
		this.score = score;
	}

	public Boolean getBeMember() {
		return beMember;
	}

	public void setBeMember(Boolean beMember) {
		this.beMember = beMember;
	}

	@Override
	public String toString() {
		return "Student [sno=" + sno + ", name=" + name + ", sex=" + sex + ", birthdayDate=" + birthdayDate + ", score="
				+ score + ", beMember=" + beMember + "]";
	}
	
}
