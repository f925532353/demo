package com.example.demo.test;

import java.io.File;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.junit.Test;

import com.example.demo.ExcelParseHelper;
import com.example.demo.entity.Student;

public class StudentTest {

	@Test
	public void testStudent() {
		File file = getFile("src/student.xls");
		List<Student> stuList = ExcelParseHelper.parse(file, Student.class, 1);
		stuList.forEach(System.out::println);
	}
	
	@Test
	public void testDate() {
		Date date = Calendar.getInstance().getTime();
		System.out.println(date.toString());
	}
	
	public File getFile(String fileName) {
		
		try {
			URI uri = StudentTest.class.getClassLoader().getResource("student.xls").toURI();
			return new File(uri);
		} catch (URISyntaxException e) {
			throw new IllegalArgumentException(e);
		}
	}
	
}
